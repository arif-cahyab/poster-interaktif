import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/models/quiz_result.dart';
import 'package:poster_interaktif/core/models/quiz_result_argument.dart';
import 'package:poster_interaktif/core/widgets/quiz_item_widget.dart';

class QuizResultPageView extends StatefulWidget {
  final QuizResultArgument quizResultArgument;

  const QuizResultPageView({Key key, this.quizResultArgument})
      : super(key: key);

  @override
  _QuizResultPageViewState createState() => _QuizResultPageViewState();
}

class _QuizResultPageViewState extends State<QuizResultPageView> {
  QuizResultArgument get quizResultArgument => widget.quizResultArgument;
  QuizResult result;

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      child: Scaffold(
        body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Image.asset(
                  ImageAssets.quizBackground,
                  fit: BoxFit.fill,
                  width: double.infinity,
                  height: double.infinity,
                ),
                _buildTotalScore(),
                _buildBackButton(),
              ],
            )),
      ),
    );
  }

  Widget _buildTotalScore() {
    return Container(
      width: MediaQuery.of(context).size.width,
      child: Column(
        crossAxisAlignment: CrossAxisAlignment.center,
        mainAxisAlignment: MainAxisAlignment.center,
        children: [
          Text(
            "Total Score Anda",
            style: TextStyle(
              fontFamily: FontAssets.josefinSansBold,
              fontSize: 24,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
              color: Colors.black,
            ),
          ),
          Text(
            widget.quizResultArgument.quizScore.toStringAsFixed(2),
            style: TextStyle(
              fontFamily: FontAssets.josefinSansBold,
              fontSize: 32,
              fontStyle: FontStyle.normal,
              fontWeight: FontWeight.bold,
              color: Colors.orange,
            ),
          )
        ],
      ),
    );
  }

  Widget _buildBackButton() {
    return Positioned(
      bottom: 30,
      right: 30,
      left: 30,
      child: QuizItemWidget(
        text: "Kembali",
        background: Colors.white,
        onTap: () {
          Navigator.pop(context);
        },
      ),
    );
  }

  @override
  void initState() {
    super.initState();
  }
}
