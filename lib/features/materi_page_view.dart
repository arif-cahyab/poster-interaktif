import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poster_interaktif/core/widgets/home_icon_widget.dart';
import 'package:poster_interaktif/core/widgets/video_controls_overlay.dart';
import 'package:video_player/video_player.dart';
import 'package:youtube_player_flutter/youtube_player_flutter.dart';

class MateriPageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _MateriPageViewState();
}

class _MateriPageViewState extends State<MateriPageView> {
  VideoPlayerController _controller;
  YoutubePlayerController _playerController;
  // final String _videoPath = "assets/video/materi.mp4";
  final String _videoPath = "https://gitlab.com/arif-cahyab/poster-interaktif/-/blob/master/video/materi.mp4";

  @override
  void initState() {
    super.initState();
    _controller = VideoPlayerController.network(_videoPath);
    _controller.addListener(() {
      setState(() {});
    });
    _controller.setLooping(true);
    _controller.initialize().then((value) => setState(() {}));
    _controller.play();

    _playerController = YoutubePlayerController(
      flags: const YoutubePlayerFlags(
        mute: false,
        autoPlay: true,
        disableDragSeek: false,
        loop: false,
        isLive: false,
        forceHD: false,
        enableCaption: true,
      ), initialVideoId: "zFH4ilu41Is",
    );
  }

  @override
  void dispose() {
    _controller.dispose();
    _playerController.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: YoutubePlayerBuilder(
        onExitFullScreen: (){
          SystemChrome.setPreferredOrientations(DeviceOrientation.values);
        },
        player: YoutubePlayer(
          controller: _playerController,
        ),
        builder: (context, player) {
          return Scaffold(
            body: Container(
              height: MediaQuery.of(context).size.height,
              child: Container(
                width: MediaQuery.of(context).size.width,
                height: MediaQuery.of(context).size.height,
                child: Column(
                  children: [
                    player,
                  ],
                ),
              ),
            ),
          );
        },
      ),
    );
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Container(
          height: MediaQuery.of(context).size.height,
          child: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: AspectRatio(
              aspectRatio: _controller.value.aspectRatio,
              child: Stack(
                alignment: Alignment.bottomCenter,
                children: [
                  VideoPlayer(_controller),
                  VideoControlsOverlay(controller: _controller),
                  VideoProgressIndicator(_controller, allowScrubbing: true),
                ],
              ),
            ),
          ),
        ),
      ),
    );
  }
}
