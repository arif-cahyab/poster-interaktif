import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/widgets/home_icon_widget.dart';

class InstructionPageView extends StatefulWidget {
  final InstructionType instructionType;

  InstructionPageView(this.instructionType);

  @override
  State<StatefulWidget> createState() => _InstructionPageViewState();
}

class _InstructionPageViewState extends State<InstructionPageView> {
  String get _imagePath => widget.instructionType == InstructionType.petunjuk
      ? ImageAssets.petunjukPenggunaanBackground
      : ImageAssets.kdDantujuanPembelajaranBackground;

  String get _soundPath => widget.instructionType == InstructionType.petunjuk
      ? SoundAssets.petunjukBacksound
      : SoundAssets.kdDanTujuanBacksound;

  AssetsAudioPlayer _audioPlayer = AssetsAudioPlayer();

  @override
  void initState() {
    super.initState();
    _audioPlayer.open(
      Audio(_soundPath),
      loopMode: LoopMode.single,
      autoStart: true,
    );
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(children: [
            Image.asset(
              _imagePath,
              fit: BoxFit.fill,
              height: double.infinity,
              width: double.infinity,
            ),
            HomeIconWidget()
          ]),
        ),
      ),
    );
  }

  @override
  void dispose() {
    super.dispose();
    _audioPlayer.stop();
    _audioPlayer.dispose();
  }
}

enum InstructionType { petunjuk, kdDanTujuan }
