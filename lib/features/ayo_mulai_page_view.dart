import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/poster_routes.dart';
import 'package:poster_interaktif/core/widgets/action_image_button.dart';

class AyoMulaiPageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _AyoMulaiPageViewState();
}

class _AyoMulaiPageViewState extends State<AyoMulaiPageView> with WidgetsBindingObserver {
  AssetsAudioPlayer _audioPlayer = AssetsAudioPlayer();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    _playBackgroundSound();
  }


  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch(state){
      case AppLifecycleState.inactive:
        _audioPlayer.pause();
        break;
      case AppLifecycleState.paused:
        _audioPlayer.pause();
        break;
      case AppLifecycleState.resumed:
        _audioPlayer.play();
        break;
      case AppLifecycleState.detached:
        _audioPlayer.stop();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(children: [
            Image.asset(
              ImageAssets.posterBackground,
              fit: BoxFit.fill,
              width: double.infinity,
              height: double.infinity,
            ),
            _buildMateriButton(),
            _buildQuizButton(),
            _buildQuizGuideButton(),
            _buildHomeIcon(),
            _buildExitIcon(),
          ]),
        ),
      ),
    );
  }

  Widget _buildMateriButton() {
    return Positioned(
      right: 30,
      top: 200,
      width: MediaQuery.of(context).size.width / 4,
      child: ActionImageButton(
        pathImage: ImageAssets.materiButton,
        onTap: () {
          _audioPlayer.open(
            Audio(SoundAssets.clickSound)
          );
          _navigateTo(PosterRoutes.materi);
        },
      ),
    );
  }

  Widget _buildQuizButton() {
    return Positioned(
        left: 30,
        top: 250,
        width: MediaQuery.of(context).size.width / 4,
        child: ActionImageButton(
          pathImage: ImageAssets.quizButton,
          onTap: () {
            _audioPlayer.open(
                Audio(SoundAssets.clickSound)
            );
            _navigateTo(PosterRoutes.quiz);
          },
        ),
    );
  }

  Widget _buildQuizGuideButton(){
    return Positioned(
      left: 30,
        top: 350,
        width: MediaQuery.of(context).size.width / 2,
        child: ActionImageButton(
          pathImage: ImageAssets.quizGuideButton,
          onTap: () {
            _audioPlayer.open(
                Audio(SoundAssets.clickSound)
            );
            _navigateTo(PosterRoutes.quizGuide);
          },
        )
    );
  }

  Widget _buildHomeIcon() {
    return Positioned(
        left: 30,
        bottom: 50,
        width: MediaQuery.of(context).size.width / 8,
        child: ActionImageButton(
          pathImage: ImageAssets.homeIcon,
          onTap: () {
            Navigator.pop(context, true);
          },
        ));
  }

  Widget _buildExitIcon() {
    return Positioned(
        right: 30,
        bottom: 50,
        width: MediaQuery.of(context).size.width / 5,
        child: ActionImageButton(
          pathImage: ImageAssets.exitIcon,
          onTap: () {
            SystemNavigator.pop();
          },
        ));
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _audioPlayer.stop();
    _audioPlayer.dispose();
  }

  void _navigateTo(String routeName, {Object argument}) {
    Navigator.pushNamed(context, routeName, arguments: argument).then((value) {
      _playBackgroundSound();
    });
  }

  void _playBackgroundSound() {
    _audioPlayer.open(
      Audio(SoundAssets.ayoMulaiBacksound),
      autoStart: true,
      loopMode: LoopMode.single,
    );
  }
}
