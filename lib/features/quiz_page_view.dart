import 'dart:async';

import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/database/app_database.dart';
import 'package:poster_interaktif/core/database/dao/question_dao.dart';
import 'package:poster_interaktif/core/injectable.dart';
import 'package:poster_interaktif/core/models/quiz_result.dart';
import 'package:poster_interaktif/core/poster_routes.dart';
import 'package:poster_interaktif/core/widgets/quiz_item_widget.dart';

class QuizPageView extends StatefulWidget {
  @override
  State<StatefulWidget> createState() => _QuizPageViewState();
}

class _QuizPageViewState extends State<QuizPageView>
    with WidgetsBindingObserver {
  AssetsAudioPlayer _assetsAudioPlayer = AssetsAudioPlayer.newPlayer();
  AssetsAudioPlayer _assetsAudioPlayerCheckAnswer = AssetsAudioPlayer.newPlayer();
  List<Choice> choices = List<Choice>();
  int correctAnswers = 0;
  List<Question> questions = List<Question>();
  int questionPosition = 0;
  int questionTotal = 0;
  int wrongAnswers = 0;
  bool usedDeleteChoices = false;
  bool usedRepeatQuestion = false;
  int tryToAnswer = 2;
  List<int> selectedChoices = List<int>();

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    _assetsAudioPlayer.open(Audio(SoundAssets.quizBacksound),
        autoStart: true, loopMode: LoopMode.single);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _assetsAudioPlayer.stop();
    _assetsAudioPlayer.dispose();
    _assetsAudioPlayerCheckAnswer.stop();
    _assetsAudioPlayerCheckAnswer.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    super.didChangeAppLifecycleState(state);
    switch (state) {
      case AppLifecycleState.inactive:
        _assetsAudioPlayer.pause();
        break;
      case AppLifecycleState.paused:
        _assetsAudioPlayer.pause();
        break;
      case AppLifecycleState.resumed:
        _assetsAudioPlayer.play();
        break;
      case AppLifecycleState.detached:
        _assetsAudioPlayer.stop();
        break;
    }
  }

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: WillPopScope(
        onWillPop: () {
          _assetsAudioPlayer.stop();
          _assetsAudioPlayerCheckAnswer.stop();
          return Future<bool>.value(true);
        },
        child: Scaffold(
          body: Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Stack(
              children: [
                Image.asset(
                  ImageAssets.quizBackground,
                  fit: BoxFit.fill,
                  width: double.infinity,
                  height: double.infinity,
                ),
                _buildContentQuiz(context)
              ],
            ),
          ),
        ),
      ),
    );
  }

  Widget _buildOptionsButton(BuildContext context, int questionId) {
    return Align(
      alignment: Alignment.bottomCenter,
      child: Container(
          margin: EdgeInsets.symmetric(vertical: 50),
          child: Row(
              mainAxisAlignment: MainAxisAlignment.center,
              crossAxisAlignment: CrossAxisAlignment.center,
              children: [
                _buildButton(
                    assetName: ImageAssets.repeatQuestion,
                    onTap: () async {
                      if (usedRepeatQuestion == false && tryToAnswer > 0) {
                        setState(() {
                          usedRepeatQuestion = true;
                          tryToAnswer -= 1;
                          questionPosition -= 1;
                          selectedChoices.removeLast();
                        });
                      } else {
                        showSnackBar(
                            context, "Kamu sudah menggunakan opsi Repeat!");
                      }
                    }),
                _buildButton(
                    assetName: ImageAssets.deleteChoices,
                    onTap: () {
                      if (usedDeleteChoices == false && tryToAnswer > 0) {
                        int caIndex = 0;
                        int choiceIndex = 0;

                        // get caIndex = correct answer index
                        for (var i = 0; i < choices.length; i++) {
                          if (choices[i].correctAnswers == 1) {
                            caIndex = i;
                            break;
                          }
                        }
                        setState(() {
                          usedDeleteChoices = true;
                          tryToAnswer -= 1;
                          if (caIndex == 0) {
                            choiceIndex = caIndex + 1;
                            choices.removeAt(2);
                            choices.removeAt(3);
                          } else if (caIndex == 3) {
                            choiceIndex = caIndex - 1;
                            choices.removeAt(0);
                            choices.removeAt(1);
                          } else {
                            choiceIndex = caIndex == 1
                                ? (choiceIndex + 1)
                                : (choiceIndex - 1);
                            choices.removeLast();
                            choices.removeAt(0);
                          }
                        });
                      } else {
                        showSnackBar(context,
                            "Kamu sudah menggunakan opsi Hapus Pilihan Jawaban!");
                      }
                    })
              ])),
    );
  }

  void showSnackBar(BuildContext context, String text) {
    SnackBar snackBar = SnackBar(
      content: Text(text),
    );
    Scaffold.of(context).showSnackBar(snackBar);
  }

  Widget _buildButton({String assetName, Function onTap}) {
    return Padding(
      padding: const EdgeInsets.symmetric(horizontal: 10.0),
      child: GestureDetector(
          onTap: onTap,
          child: Image.asset(
            assetName,
            width: 50,
            height: 50,
          )),
    );
  }

  Widget _buildContentQuiz(BuildContext context) {
    return SingleChildScrollView(
      child: StreamBuilder(
        stream: locator<QuestionDao>().selectQuestionStream(),
        initialData: questions,
        builder:
            (BuildContext context, AsyncSnapshot<List<Question>> snapshot) {
          questions = snapshot.data;

          return questions.length > 0
              ? Column(
                  children: [
                    _buildNumberOfQuestion(questions.length),
                    _buildQuestionsOfQuiz(),
                    _buildChoiceAnswers(questions[questionPosition].id),
                    _buildOptionsButton(
                        context, questions[questionPosition].id),
                  ],
                )
              : _buildCircularLoader();
        },
      ),
    );
  }

  Widget _buildCircularLoader() {
    return Center(
      child: SizedBox(
        width: 50,
        height: 50,
        child: CircularProgressIndicator(),
      ),
    );
  }

  Widget _buildNumberOfQuestion(int questionTotal) {
    return Container(
      margin: EdgeInsets.symmetric(vertical: 20),
      width: MediaQuery.of(context).size.width,
      child: Text(
        "Soal ${(questionPosition + 1)}/$questionTotal",
        style: TextStyle(
          fontWeight: FontWeight.w900,
          fontSize: 18,
          fontFamily: FontAssets.josefinSansBold,
        ),
        textAlign: TextAlign.center,
      ),
    );
  }

  Widget _buildChoiceAnswers(int questionId) {
    return StreamBuilder(
      stream: locator<QuestionDao>().selectChoiceByQuestionIdStream(questionId),
      initialData: choices,
      builder: (BuildContext context, AsyncSnapshot<List<Choice>> snapshot) {
        choices = snapshot.data;
        questionTotal = choices.length;

        return choices.length > 0
            ? Container(
                width: MediaQuery.of(context).size.width - 100,
                child: Column(
                  children: choices
                      .map(
                        (choice) => QuizItemWidget(
                          text: choice.answerChoices,
                          onTap: () {
                            _checkAnswer(choice);
                          },
                        ),
                      )
                      .toList(),
                ))
            : _buildCircularLoader();
      },
    );
  }

  Widget _buildQuestionsOfQuiz() {
    return Container(
      width: MediaQuery.of(context).size.width - 100,
      margin: EdgeInsets.only(bottom: 20),
      child: QuizItemWidget(
        text: questions[questionPosition].question,
        imageDecoded: questions[questionPosition].image,
      ),
    );
  }

  void _checkAnswer(Choice choice) {
    bool isCorrectAnswer = choice.correctAnswers == 1;
    correctAnswers = isCorrectAnswer ? correctAnswers + 1 : correctAnswers;
    wrongAnswers = isCorrectAnswer ? wrongAnswers : wrongAnswers + 1;
    selectedChoices.add(correctAnswers);

    String soundAssets =
        isCorrectAnswer ? SoundAssets.correctAnswer : SoundAssets.wrongAnswer;
    _assetsAudioPlayerCheckAnswer.open(Audio(soundAssets));

    String imageAsset =
        isCorrectAnswer ? ImageAssets.correctAnswer : ImageAssets.wrongAnswer;

    Timer timer;
    showDialog(
      context: context,
      barrierDismissible: false,
      builder: (BuildContext context) {
        timer = Timer(Duration(milliseconds: 1500), () {
          Navigator.pop(context);
          int questionsOfArray = questions.length - 1;
          print("isCorrect $isCorrectAnswer || $tryToAnswer");
          if (questionPosition < questionsOfArray) {
            setState(() {
              questionPosition += 1;
            });
          } else if (questionPosition == questionsOfArray) {
            QuizResult result = QuizResult(
              correctAnswers: this.correctAnswers,
              wrongAnswers: this.wrongAnswers,
              questionsTotal: this.questions.length,
            );

            Navigator.pushReplacementNamed(
              context,
              PosterRoutes.quizResult,
              arguments: result.convertToArgument(),
            );
          }
        });

        return Dialog(
          backgroundColor: Colors.white,
          elevation: 3,
          child: Image.asset(
            imageAsset,
            width: 200,
            height: 200,
          ),
        );
      },
    ).then((value) {
      if (timer.isActive) {
        timer.cancel();
      }
    });
  }
}
