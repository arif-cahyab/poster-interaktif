import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:flutter/services.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/database/initialize_database.dart';
import 'package:poster_interaktif/core/poster_routes.dart';
import 'package:poster_interaktif/core/widgets/action_image_button.dart';
import 'package:poster_interaktif/features/instruction_page_view.dart';

class HomePageView extends StatefulWidget {
  @override
  _HomePageViewState createState() => _HomePageViewState();
}

class _HomePageViewState extends State<HomePageView>
    with WidgetsBindingObserver {
  final AssetsAudioPlayer _audioPlayer = AssetsAudioPlayer();

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Stack(children: [
          Container(
            width: MediaQuery.of(context).size.width,
            height: MediaQuery.of(context).size.height,
            child: Image.asset(
              ImageAssets.homeBackground,
              fit: BoxFit.fill,
            ),
          ),
          Positioned(
            child: _buildMenu(),
            top: 200,
            left: 50,
            width: MediaQuery.of(context).size.width - 100,
          ),
          _buildExitButton()
        ]),
      ),
    );
  }

  Widget _buildExitButton() {
    return Positioned(
      child: ActionImageButton(
        pathImage: ImageAssets.exitIcon,
        onTap: () {
          SystemNavigator.pop();
        },
      ),
      bottom: 50,
      right: 30,
      width: MediaQuery.of(context).size.width / 5,
    );
  }

  Widget _buildMenu() {
    return Column(
      children: [
        ActionImageButton(
            pathImage: ImageAssets.ayoMulaiButton,
            onTap: () async {
              _audioPlayer.open(Audio(SoundAssets.clickSound));
              _navigateTo(PosterRoutes.ayoMulai);
            }),
        ActionImageButton(
            pathImage: ImageAssets.kdDanTujuanButton,
            onTap: () {
              _audioPlayer.open(Audio(SoundAssets.clickSound));
              _navigateTo(
                PosterRoutes.instruction,
                argument: InstructionType.kdDanTujuan,
              );
            }),
        ActionImageButton(
            pathImage: ImageAssets.petunjukPenggunaanButton,
            onTap: () {
              _audioPlayer.open(Audio(SoundAssets.clickSound));
              _navigateTo(
                PosterRoutes.instruction,
                argument: InstructionType.petunjuk,
              );
            }),
      ],
    );
  }

  @override
  void initState() {
    WidgetsBinding.instance.addObserver(this);
    super.initState();
    InitializeDatabase initializeDatabase = InitializeDatabase();
    initializeDatabase.initializeDatabaseExist();
    _playBackgroundSound();
  }

  void _playBackgroundSound() {
    _audioPlayer.open(Audio(SoundAssets.homeBacksound),
        autoStart: true, loopMode: LoopMode.single);
  }

  @override
  void dispose() {
    WidgetsBinding.instance.removeObserver(this);
    super.dispose();
    _audioPlayer.stop();
    _audioPlayer.dispose();
  }

  @override
  void didChangeAppLifecycleState(AppLifecycleState state) {
    switch (state) {
      case AppLifecycleState.inactive:
        _audioPlayer.pause();
        break;
      case AppLifecycleState.resumed:
        _audioPlayer.play();
        break;
      case AppLifecycleState.paused:
        _audioPlayer.pause();
        break;
      case AppLifecycleState.detached:
        _audioPlayer.stop();
        break;
    }
  }

  void _navigateTo(String routeName, {Object argument}) {
    Navigator.pushNamed(context, routeName, arguments: argument).then((value) {
      _playBackgroundSound();
    });
  }
}
