import 'package:flutter/cupertino.dart';
import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/widgets/home_icon_widget.dart';

class QuizGuidePageView extends StatelessWidget {
  final List<String> guideForQuiz = [
    "PETUNJUK KUIS",
    "Kuis ini terdiri dari 15 soal dengan 4 pilihan jawaban setiap soal.",
    "1. Bacalah pertanyaan dengan seksama.",
    "2. Pilihlah jawaban yang paling tepat dengan klik kotak jawaban.",
    "3. Terdapat 2 kekuatan yang dapat digunakan, kekuatan \"Repeat\" "
        "memberikan kesempatan untuk menjawab soal kembali dan kekuatan "
        "\"Delete\" akan menghapus 2 pilihan jawaban yang salah sehingga "
        "peluang untuk menjawab soal benar lebih besar.",
    "Semoga beruntung😊"
  ];

  @override
  Widget build(BuildContext context) {
    return SafeArea(
      top: true,
      child: Scaffold(
        body: Container(
          width: MediaQuery.of(context).size.width,
          height: MediaQuery.of(context).size.height,
          child: Stack(children: [
            Image.asset(
              ImageAssets.quizBackground,
              fit: BoxFit.fill,
              width: double.infinity,
              height: double.infinity,
            ),
            _buildGuide(context),
            HomeIconWidget()
          ]),
        ),
      ),
    );
  }

  Widget _buildGuide(BuildContext context) {
    return Positioned(
      top: 100,
      left: 20,
      right: 20,
      child: Container(
        decoration: BoxDecoration(
          color: Colors.white70,
          borderRadius: BorderRadius.circular(20),
        ),
        padding: EdgeInsets.all(20),
        child: Column(
          crossAxisAlignment: CrossAxisAlignment.start,
          children: guideForQuiz.map(
            (guide) {
              int index = guideForQuiz.indexOf(guide);
              Text text;
              if (index == 0) {
                text = Text(
                  "$guide\n\n",
                  style: TextStyle(
                      fontFamily: FontAssets.josefinSansBold,
                      fontSize: 24,
                      fontWeight: FontWeight.bold),
                  textAlign: TextAlign.center,
                );
              } else if (index == (guideForQuiz.length - 1)) {
                text = Text(
                  "\n\n$guide",
                  style: TextStyle(
                    fontWeight: FontWeight.bold,
                    fontSize: 24,
                  ),
                  textAlign: TextAlign.center,
                );
              } else {
                text = Text(
                  "$guide\n",
                  textAlign: TextAlign.left,
                  style: TextStyle(
                    fontSize: 18,
                  ),
                );
              }

              return Container(
                width: MediaQuery.of(context).size.width,
                child: text,
              );
            },
          ).toList(),
        ),
      ),
    );
  }
}
