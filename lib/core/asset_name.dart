class ImageAssets {
  static const String _pathImage = "assets/images";
  static const String _pathButton = "$_pathImage/button";
  static const String _pathIcon = "$_pathImage/icon";
  
  /// background image and poster
  static const String homeBackground = "$_pathImage/home_background.jpg";
  static const String kdDantujuanPembelajaranBackground = "$_pathImage/kd_dan_tujuan_pembelajaran_background.jpg";
  static const String petunjukPenggunaanBackground = "$_pathImage/petunjuk_penggunaan_background.png";
  static const String posterBackground = "$_pathImage/poster_background.gif";
  static const String quizBackground = "$_pathImage/quiz_background.png";
  
  /// icon
  static const String exitIcon = "$_pathIcon/exit.png";
  static const String homeIcon = "$_pathIcon/home.png";
  static const String correctAnswer = "$_pathIcon/correct_answer.gif";
  static const String wrongAnswer = "$_pathIcon/wrong_answer.gif";
  static const String deleteChoices = "$_pathIcon/delete_some_choices.png";
  static const String repeatQuestion = "$_pathIcon/repeat_question.png";
  
  /// button
  static const String ayoMulaiButton = "$_pathButton/ayo_mulai_button.png";
  static const String kdDanTujuanButton = "$_pathButton/kd_dan_tujuan_pembelajaran_button.png";
  static const String materiButton = "$_pathButton/materi_button.png";
  static const String petunjukPenggunaanButton = "$_pathButton/petunjuk_penggunaan_button.png";
  static const String quizButton = "$_pathButton/quiz_button.png";
  static const String quizGuideButton = "$_pathButton/quiz_guide_button.png";
  
}

class SoundAssets {
  static const String _pathSound = "assets/sound";
  
  static const String ayoMulaiBacksound = "$_pathSound/page_ayo_mulai_backsound.mp3";
  static const String homeBacksound = "$_pathSound/page_home_backsound.mp3";
  static const String kdDanTujuanBacksound = "$_pathSound/kd_dan_tujuan_backsound.mp3";
  static const String clickSound = "$_pathSound/klik.mp3";
  static const String petunjukBacksound = "$_pathSound/petunjuk_backsound.mp3";
  static const String correctAnswer = "$_pathSound/correct_answer.mp3";
  static const String wrongAnswer = "$_pathSound/wrong_answer.mp3";
  static const String quizBacksound = "$_pathSound/quiz_backsound.mp3";
}

class DatabaseAssets {
  static const String pathDatabase = "assets/database";
  static const String databaseName = "poster_interaktif.db";
}

class FontAssets {
  static const String josefinSansRegular = "JosefinSansRegular";
  static const String josefinSansBold = "JosefinSansBold";
}