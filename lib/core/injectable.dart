import 'package:get_it/get_it.dart';
import 'package:poster_interaktif/core/database/app_database.dart';
import 'package:poster_interaktif/core/database/dao/question_dao.dart';

final locator = GetIt.instance;

void setupLocator() {
  locator.registerLazySingleton<AppDatabase>(() => AppDatabase());
  locator
      .registerFactory<QuestionDao>(() => QuestionDao(locator<AppDatabase>()));
}
