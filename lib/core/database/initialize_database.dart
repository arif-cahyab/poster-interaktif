import 'dart:io';
import 'dart:ui';

import 'package:flutter/services.dart';
import 'package:path/path.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:sqflite/sqflite.dart';

class InitializeDatabase {

  int version = 0;

  Future<bool> initializeDatabaseExist() async {
    String databasePath = await getDatabasesPath();
    String path = join(databasePath, DatabaseAssets.databaseName);
    bool isExist = await File(path).exists();
    if (!isExist) {
      ByteData data = await rootBundle.load(join(DatabaseAssets.pathDatabase, DatabaseAssets.databaseName));
      List<int> bytes = data.buffer.asUint8List(data.offsetInBytes, data.lengthInBytes);
      await new File(path).writeAsBytes(bytes);
      return true;
    }
    return false;
  }
}
