// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'app_database.dart';

// **************************************************************************
// MoorGenerator
// **************************************************************************

// ignore_for_file: unnecessary_brace_in_string_interps, unnecessary_this
class Question extends DataClass implements Insertable<Question> {
  final int id;
  final int no;
  final String question;
  final Uint8List image;
  Question(
      {@required this.id,
      @required this.no,
      @required this.question,
      @required this.image});
  factory Question.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    final uint8ListType = db.typeSystem.forDartType<Uint8List>();
    return Question(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      no: intType.mapFromDatabaseResponse(data['${effectivePrefix}no']),
      question: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}question']),
      image: uint8ListType
          .mapFromDatabaseResponse(data['${effectivePrefix}image']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || no != null) {
      map['no'] = Variable<int>(no);
    }
    if (!nullToAbsent || question != null) {
      map['question'] = Variable<String>(question);
    }
    if (!nullToAbsent || image != null) {
      map['image'] = Variable<Uint8List>(image);
    }
    return map;
  }

  QuestionsCompanion toCompanion(bool nullToAbsent) {
    return QuestionsCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      no: no == null && nullToAbsent ? const Value.absent() : Value(no),
      question: question == null && nullToAbsent
          ? const Value.absent()
          : Value(question),
      image:
          image == null && nullToAbsent ? const Value.absent() : Value(image),
    );
  }

  factory Question.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Question(
      id: serializer.fromJson<int>(json['id']),
      no: serializer.fromJson<int>(json['no']),
      question: serializer.fromJson<String>(json['question']),
      image: serializer.fromJson<Uint8List>(json['image']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'no': serializer.toJson<int>(no),
      'question': serializer.toJson<String>(question),
      'image': serializer.toJson<Uint8List>(image),
    };
  }

  Question copyWith({int id, int no, String question, Uint8List image}) =>
      Question(
        id: id ?? this.id,
        no: no ?? this.no,
        question: question ?? this.question,
        image: image ?? this.image,
      );
  @override
  String toString() {
    return (StringBuffer('Question(')
          ..write('id: $id, ')
          ..write('no: $no, ')
          ..write('question: $question, ')
          ..write('image: $image')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(id.hashCode,
      $mrjc(no.hashCode, $mrjc(question.hashCode, image.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Question &&
          other.id == this.id &&
          other.no == this.no &&
          other.question == this.question &&
          other.image == this.image);
}

class QuestionsCompanion extends UpdateCompanion<Question> {
  final Value<int> id;
  final Value<int> no;
  final Value<String> question;
  final Value<Uint8List> image;
  const QuestionsCompanion({
    this.id = const Value.absent(),
    this.no = const Value.absent(),
    this.question = const Value.absent(),
    this.image = const Value.absent(),
  });
  QuestionsCompanion.insert({
    this.id = const Value.absent(),
    @required int no,
    @required String question,
    @required Uint8List image,
  })  : no = Value(no),
        question = Value(question),
        image = Value(image);
  static Insertable<Question> custom({
    Expression<int> id,
    Expression<int> no,
    Expression<String> question,
    Expression<Uint8List> image,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (no != null) 'no': no,
      if (question != null) 'question': question,
      if (image != null) 'image': image,
    });
  }

  QuestionsCompanion copyWith(
      {Value<int> id,
      Value<int> no,
      Value<String> question,
      Value<Uint8List> image}) {
    return QuestionsCompanion(
      id: id ?? this.id,
      no: no ?? this.no,
      question: question ?? this.question,
      image: image ?? this.image,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (no.present) {
      map['no'] = Variable<int>(no.value);
    }
    if (question.present) {
      map['question'] = Variable<String>(question.value);
    }
    if (image.present) {
      map['image'] = Variable<Uint8List>(image.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('QuestionsCompanion(')
          ..write('id: $id, ')
          ..write('no: $no, ')
          ..write('question: $question, ')
          ..write('image: $image')
          ..write(')'))
        .toString();
  }
}

class $QuestionsTable extends Questions
    with TableInfo<$QuestionsTable, Question> {
  final GeneratedDatabase _db;
  final String _alias;
  $QuestionsTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _noMeta = const VerificationMeta('no');
  GeneratedIntColumn _no;
  @override
  GeneratedIntColumn get no => _no ??= _constructNo();
  GeneratedIntColumn _constructNo() {
    return GeneratedIntColumn(
      'no',
      $tableName,
      false,
    );
  }

  final VerificationMeta _questionMeta = const VerificationMeta('question');
  GeneratedTextColumn _question;
  @override
  GeneratedTextColumn get question => _question ??= _constructQuestion();
  GeneratedTextColumn _constructQuestion() {
    return GeneratedTextColumn(
      'question',
      $tableName,
      false,
    );
  }

  final VerificationMeta _imageMeta = const VerificationMeta('image');
  GeneratedBlobColumn _image;
  @override
  GeneratedBlobColumn get image => _image ??= _constructImage();
  GeneratedBlobColumn _constructImage() {
    return GeneratedBlobColumn(
      'image',
      $tableName,
      false,
    );
  }

  @override
  List<GeneratedColumn> get $columns => [id, no, question, image];
  @override
  $QuestionsTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'questions';
  @override
  final String actualTableName = 'questions';
  @override
  VerificationContext validateIntegrity(Insertable<Question> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('no')) {
      context.handle(_noMeta, no.isAcceptableOrUnknown(data['no'], _noMeta));
    } else if (isInserting) {
      context.missing(_noMeta);
    }
    if (data.containsKey('question')) {
      context.handle(_questionMeta,
          question.isAcceptableOrUnknown(data['question'], _questionMeta));
    } else if (isInserting) {
      context.missing(_questionMeta);
    }
    if (data.containsKey('image')) {
      context.handle(
          _imageMeta, image.isAcceptableOrUnknown(data['image'], _imageMeta));
    } else if (isInserting) {
      context.missing(_imageMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Question map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Question.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $QuestionsTable createAlias(String alias) {
    return $QuestionsTable(_db, alias);
  }
}

class Choice extends DataClass implements Insertable<Choice> {
  final int id;
  final int questionId;
  final String answerChoices;
  final int correctAnswers;
  Choice(
      {@required this.id,
      @required this.questionId,
      @required this.answerChoices,
      @required this.correctAnswers});
  factory Choice.fromData(Map<String, dynamic> data, GeneratedDatabase db,
      {String prefix}) {
    final effectivePrefix = prefix ?? '';
    final intType = db.typeSystem.forDartType<int>();
    final stringType = db.typeSystem.forDartType<String>();
    return Choice(
      id: intType.mapFromDatabaseResponse(data['${effectivePrefix}id']),
      questionId: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}question_id']),
      answerChoices: stringType
          .mapFromDatabaseResponse(data['${effectivePrefix}answer_choices']),
      correctAnswers: intType
          .mapFromDatabaseResponse(data['${effectivePrefix}correct_answers']),
    );
  }
  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (!nullToAbsent || id != null) {
      map['id'] = Variable<int>(id);
    }
    if (!nullToAbsent || questionId != null) {
      map['question_id'] = Variable<int>(questionId);
    }
    if (!nullToAbsent || answerChoices != null) {
      map['answer_choices'] = Variable<String>(answerChoices);
    }
    if (!nullToAbsent || correctAnswers != null) {
      map['correct_answers'] = Variable<int>(correctAnswers);
    }
    return map;
  }

  ChoicesCompanion toCompanion(bool nullToAbsent) {
    return ChoicesCompanion(
      id: id == null && nullToAbsent ? const Value.absent() : Value(id),
      questionId: questionId == null && nullToAbsent
          ? const Value.absent()
          : Value(questionId),
      answerChoices: answerChoices == null && nullToAbsent
          ? const Value.absent()
          : Value(answerChoices),
      correctAnswers: correctAnswers == null && nullToAbsent
          ? const Value.absent()
          : Value(correctAnswers),
    );
  }

  factory Choice.fromJson(Map<String, dynamic> json,
      {ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return Choice(
      id: serializer.fromJson<int>(json['id']),
      questionId: serializer.fromJson<int>(json['questionId']),
      answerChoices: serializer.fromJson<String>(json['answerChoices']),
      correctAnswers: serializer.fromJson<int>(json['correctAnswers']),
    );
  }
  @override
  Map<String, dynamic> toJson({ValueSerializer serializer}) {
    serializer ??= moorRuntimeOptions.defaultSerializer;
    return <String, dynamic>{
      'id': serializer.toJson<int>(id),
      'questionId': serializer.toJson<int>(questionId),
      'answerChoices': serializer.toJson<String>(answerChoices),
      'correctAnswers': serializer.toJson<int>(correctAnswers),
    };
  }

  Choice copyWith(
          {int id, int questionId, String answerChoices, int correctAnswers}) =>
      Choice(
        id: id ?? this.id,
        questionId: questionId ?? this.questionId,
        answerChoices: answerChoices ?? this.answerChoices,
        correctAnswers: correctAnswers ?? this.correctAnswers,
      );
  @override
  String toString() {
    return (StringBuffer('Choice(')
          ..write('id: $id, ')
          ..write('questionId: $questionId, ')
          ..write('answerChoices: $answerChoices, ')
          ..write('correctAnswers: $correctAnswers')
          ..write(')'))
        .toString();
  }

  @override
  int get hashCode => $mrjf($mrjc(
      id.hashCode,
      $mrjc(questionId.hashCode,
          $mrjc(answerChoices.hashCode, correctAnswers.hashCode))));
  @override
  bool operator ==(dynamic other) =>
      identical(this, other) ||
      (other is Choice &&
          other.id == this.id &&
          other.questionId == this.questionId &&
          other.answerChoices == this.answerChoices &&
          other.correctAnswers == this.correctAnswers);
}

class ChoicesCompanion extends UpdateCompanion<Choice> {
  final Value<int> id;
  final Value<int> questionId;
  final Value<String> answerChoices;
  final Value<int> correctAnswers;
  const ChoicesCompanion({
    this.id = const Value.absent(),
    this.questionId = const Value.absent(),
    this.answerChoices = const Value.absent(),
    this.correctAnswers = const Value.absent(),
  });
  ChoicesCompanion.insert({
    this.id = const Value.absent(),
    @required int questionId,
    @required String answerChoices,
    @required int correctAnswers,
  })  : questionId = Value(questionId),
        answerChoices = Value(answerChoices),
        correctAnswers = Value(correctAnswers);
  static Insertable<Choice> custom({
    Expression<int> id,
    Expression<int> questionId,
    Expression<String> answerChoices,
    Expression<int> correctAnswers,
  }) {
    return RawValuesInsertable({
      if (id != null) 'id': id,
      if (questionId != null) 'question_id': questionId,
      if (answerChoices != null) 'answer_choices': answerChoices,
      if (correctAnswers != null) 'correct_answers': correctAnswers,
    });
  }

  ChoicesCompanion copyWith(
      {Value<int> id,
      Value<int> questionId,
      Value<String> answerChoices,
      Value<int> correctAnswers}) {
    return ChoicesCompanion(
      id: id ?? this.id,
      questionId: questionId ?? this.questionId,
      answerChoices: answerChoices ?? this.answerChoices,
      correctAnswers: correctAnswers ?? this.correctAnswers,
    );
  }

  @override
  Map<String, Expression> toColumns(bool nullToAbsent) {
    final map = <String, Expression>{};
    if (id.present) {
      map['id'] = Variable<int>(id.value);
    }
    if (questionId.present) {
      map['question_id'] = Variable<int>(questionId.value);
    }
    if (answerChoices.present) {
      map['answer_choices'] = Variable<String>(answerChoices.value);
    }
    if (correctAnswers.present) {
      map['correct_answers'] = Variable<int>(correctAnswers.value);
    }
    return map;
  }

  @override
  String toString() {
    return (StringBuffer('ChoicesCompanion(')
          ..write('id: $id, ')
          ..write('questionId: $questionId, ')
          ..write('answerChoices: $answerChoices, ')
          ..write('correctAnswers: $correctAnswers')
          ..write(')'))
        .toString();
  }
}

class $ChoicesTable extends Choices with TableInfo<$ChoicesTable, Choice> {
  final GeneratedDatabase _db;
  final String _alias;
  $ChoicesTable(this._db, [this._alias]);
  final VerificationMeta _idMeta = const VerificationMeta('id');
  GeneratedIntColumn _id;
  @override
  GeneratedIntColumn get id => _id ??= _constructId();
  GeneratedIntColumn _constructId() {
    return GeneratedIntColumn(
      'id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _questionIdMeta = const VerificationMeta('questionId');
  GeneratedIntColumn _questionId;
  @override
  GeneratedIntColumn get questionId => _questionId ??= _constructQuestionId();
  GeneratedIntColumn _constructQuestionId() {
    return GeneratedIntColumn(
      'question_id',
      $tableName,
      false,
    );
  }

  final VerificationMeta _answerChoicesMeta =
      const VerificationMeta('answerChoices');
  GeneratedTextColumn _answerChoices;
  @override
  GeneratedTextColumn get answerChoices =>
      _answerChoices ??= _constructAnswerChoices();
  GeneratedTextColumn _constructAnswerChoices() {
    return GeneratedTextColumn(
      'answer_choices',
      $tableName,
      false,
    );
  }

  final VerificationMeta _correctAnswersMeta =
      const VerificationMeta('correctAnswers');
  GeneratedIntColumn _correctAnswers;
  @override
  GeneratedIntColumn get correctAnswers =>
      _correctAnswers ??= _constructCorrectAnswers();
  GeneratedIntColumn _constructCorrectAnswers() {
    return GeneratedIntColumn('correct_answers', $tableName, false,
        $customConstraints: 'NULL REFERENCES questions(id)');
  }

  @override
  List<GeneratedColumn> get $columns =>
      [id, questionId, answerChoices, correctAnswers];
  @override
  $ChoicesTable get asDslTable => this;
  @override
  String get $tableName => _alias ?? 'choices';
  @override
  final String actualTableName = 'choices';
  @override
  VerificationContext validateIntegrity(Insertable<Choice> instance,
      {bool isInserting = false}) {
    final context = VerificationContext();
    final data = instance.toColumns(true);
    if (data.containsKey('id')) {
      context.handle(_idMeta, id.isAcceptableOrUnknown(data['id'], _idMeta));
    }
    if (data.containsKey('question_id')) {
      context.handle(
          _questionIdMeta,
          questionId.isAcceptableOrUnknown(
              data['question_id'], _questionIdMeta));
    } else if (isInserting) {
      context.missing(_questionIdMeta);
    }
    if (data.containsKey('answer_choices')) {
      context.handle(
          _answerChoicesMeta,
          answerChoices.isAcceptableOrUnknown(
              data['answer_choices'], _answerChoicesMeta));
    } else if (isInserting) {
      context.missing(_answerChoicesMeta);
    }
    if (data.containsKey('correct_answers')) {
      context.handle(
          _correctAnswersMeta,
          correctAnswers.isAcceptableOrUnknown(
              data['correct_answers'], _correctAnswersMeta));
    } else if (isInserting) {
      context.missing(_correctAnswersMeta);
    }
    return context;
  }

  @override
  Set<GeneratedColumn> get $primaryKey => {id};
  @override
  Choice map(Map<String, dynamic> data, {String tablePrefix}) {
    final effectivePrefix = tablePrefix != null ? '$tablePrefix.' : null;
    return Choice.fromData(data, _db, prefix: effectivePrefix);
  }

  @override
  $ChoicesTable createAlias(String alias) {
    return $ChoicesTable(_db, alias);
  }
}

abstract class _$AppDatabase extends GeneratedDatabase {
  _$AppDatabase(QueryExecutor e) : super(SqlTypeSystem.defaultInstance, e);
  $QuestionsTable _questions;
  $QuestionsTable get questions => _questions ??= $QuestionsTable(this);
  $ChoicesTable _choices;
  $ChoicesTable get choices => _choices ??= $ChoicesTable(this);
  QuestionDao _questionDao;
  QuestionDao get questionDao =>
      _questionDao ??= QuestionDao(this as AppDatabase);
  @override
  Iterable<TableInfo> get allTables => allSchemaEntities.whereType<TableInfo>();
  @override
  List<DatabaseSchemaEntity> get allSchemaEntities => [questions, choices];
}
