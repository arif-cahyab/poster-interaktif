import 'package:moor_flutter/moor_flutter.dart';
import 'package:poster_interaktif/core/database/app_database.dart';
import 'package:poster_interaktif/core/database/tables/choices.dart';
import 'package:poster_interaktif/core/database/tables/questions.dart';

part 'question_dao.g.dart';

@UseDao(tables: [Questions, Choices])
class QuestionDao extends DatabaseAccessor<AppDatabase>
    with _$QuestionDaoMixin {
  AppDatabase appDatabase;

  QuestionDao(this.appDatabase) : super(appDatabase);

  Future<List<Question>> selectQuestion() => select(questions).get();

  Future<List<Choice>> selectChoicesByQuestionId(int questionId) =>
      (select(choices)..where((choice) => choice.questionId.equals(questionId)))
          .get();

  Stream<List<Question>> selectQuestionStream() => select(questions).watch();

  Stream<List<Choice>> selectChoiceByQuestionIdStream(int questionId) =>
      (select(choices)..where((choice) => choice.questionId.equals(questionId)))
          .watch();

}
