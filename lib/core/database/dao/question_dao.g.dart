// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'question_dao.dart';

// **************************************************************************
// DaoGenerator
// **************************************************************************

mixin _$QuestionDaoMixin on DatabaseAccessor<AppDatabase> {
  $QuestionsTable get questions => attachedDatabase.questions;
  $ChoicesTable get choices => attachedDatabase.choices;
}
