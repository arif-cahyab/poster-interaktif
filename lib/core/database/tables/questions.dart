import 'package:moor_flutter/moor_flutter.dart';

class Questions extends Table {
  IntColumn get id => integer()();
  IntColumn get no => integer()();
  TextColumn get question => text()();
  BlobColumn get image => blob()();

  @override
  Set<Column> get primaryKey => {id};
}