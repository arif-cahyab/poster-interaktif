import 'package:moor_flutter/moor_flutter.dart';

class Choices extends Table {
  IntColumn get id => integer()();
  IntColumn get questionId => integer().named("question_id")();
  TextColumn get answerChoices => text().named("answer_choices")();
  IntColumn get correctAnswers => integer().named("correct_answers")
      .customConstraint("NULL REFERENCES questions(id)")();

  @override
  Set<Column> get primaryKey => {id};


}