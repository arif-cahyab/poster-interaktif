
import 'package:moor_flutter/moor_flutter.dart';
import 'package:poster_interaktif/core/asset_name.dart';
import 'package:poster_interaktif/core/database/dao/question_dao.dart';
import 'package:poster_interaktif/core/database/tables/choices.dart';

import 'tables/questions.dart';

part 'app_database.g.dart';

@UseMoor(tables: [Questions, Choices],daos: [QuestionDao])
class AppDatabase extends _$AppDatabase {
  AppDatabase()
      : super((FlutterQueryExecutor.inDatabaseFolder(
            path: DatabaseAssets.databaseName, logStatements: true)));

  @override
  int get schemaVersion => 1;

  @override
  MigrationStrategy get migration {
    return MigrationStrategy(
      beforeOpen: (details) async {
        await customStatement('PRAGMA synchronous = OFF');
        await customStatement('PRAGMA foreign_keys = ON');
      }
    );
  }
}
