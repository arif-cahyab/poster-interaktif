import 'package:flutter/material.dart';

import '../asset_name.dart';
import 'action_image_button.dart';

class HomeIconWidget extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Positioned(
      left: 10,
      bottom: 15,
      width: MediaQuery.of(context).size.width / 8,
      child: ActionImageButton(
        pathImage: ImageAssets.homeIcon,
        onTap: () {
          Navigator.pop(context, true);
        },
      ),
    );
  }
}
