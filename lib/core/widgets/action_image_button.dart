import 'package:assets_audio_player/assets_audio_player.dart';
import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/asset_name.dart';

class ActionImageButton extends StatelessWidget {
  final String pathImage;
  final Function onTap;
  final double height;
  final double width;


  ActionImageButton(
      {@required this.pathImage,
      @required this.onTap,
      this.height = 0,
      this.width = 0});

  final AssetsAudioPlayer _audioPlayer = AssetsAudioPlayer();

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      child: height == 0 && width == 0
          ? Image.asset(pathImage)
          : Image.asset(
              pathImage,
              width: width,
              height: height,
            ),
      onTap: this.onTap,
    );
  }
}
