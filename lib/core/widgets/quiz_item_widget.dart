import 'dart:typed_data';

import 'package:flutter/material.dart';
import 'package:flutter/widgets.dart';

class QuizItemWidget extends StatefulWidget {
  final String text;
  final Color background;
  final Function onTap;
  final Uint8List imageDecoded;

  QuizItemWidget({@required this.text, this.background, this.onTap, this.imageDecoded});

  @override
  _QuizItemWidgetState createState() => _QuizItemWidgetState();
}

class _QuizItemWidgetState extends State<QuizItemWidget> {
  Color backgroundColor = Colors.white;

  @override
  Widget build(BuildContext context) {
    return GestureDetector(
      onTap: widget.onTap,
      child: Container(
        decoration: BoxDecoration(
          border: Border.all(
            color: Colors.orange,
            style: BorderStyle.solid,
            width: 3.0,
          ),
          borderRadius: BorderRadius.circular(10.0),
          color: backgroundColor,
        ),
        padding: EdgeInsets.all(5.0),
        margin: EdgeInsets.symmetric(vertical: 5.0),
        child: Container(
          decoration: BoxDecoration(
            border: Border.all(
              color: Colors.orange,
              style: BorderStyle.solid,
              width: 1.0
            ),
            borderRadius: BorderRadius.circular(5.0)
          ),
          padding: EdgeInsets.symmetric(horizontal: 10.0, vertical: 20.0),
          child: _buildTextContent(),
        ),
      ),
    );
  }

  Widget _buildTextContent(){
    if (widget.imageDecoded == null){
      return Text(widget.text, textAlign: TextAlign.center,);
    } else {
      return Column(
        children: [
          Text(widget.text, textAlign: TextAlign.center,),
          Image.memory(widget.imageDecoded)
        ],
      );
    }
  }
}
