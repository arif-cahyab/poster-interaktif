class QuizResultArgument {
  final int correctAnswers;
  final int wrongAnswers;
  final int questionsTotal;
  final double quizScore;

  QuizResultArgument({
    this.correctAnswers,
    this.wrongAnswers,
    this.questionsTotal,
    this.quizScore,
  });
}
