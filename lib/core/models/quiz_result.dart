import 'package:poster_interaktif/core/models/quiz_result_argument.dart';

class QuizResult {
  final int correctAnswers;
  final int questionsTotal;
  final int wrongAnswers;

  QuizResult({
    this.correctAnswers,
    this.questionsTotal,
    this.wrongAnswers,
  });

  double calculateScore() {
    double score = (correctAnswers / questionsTotal) * 100;
    return score;
  }

  QuizResultArgument convertToArgument(){
    return QuizResultArgument(
      correctAnswers: this.correctAnswers,
      questionsTotal: this.questionsTotal,
      wrongAnswers: this.wrongAnswers,
      quizScore: this.calculateScore(),
    );
  }
}
