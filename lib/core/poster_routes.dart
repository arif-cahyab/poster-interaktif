import 'package:flutter/material.dart';
import 'package:poster_interaktif/core/models/quiz_result_argument.dart';
import 'package:poster_interaktif/features/ayo_mulai_page_view.dart';
import 'package:poster_interaktif/features/quiz_guide_page_view.dart';
import 'package:poster_interaktif/features/instruction_page_view.dart';
import 'package:poster_interaktif/features/home_page_view.dart';
import 'package:poster_interaktif/features/materi_page_view.dart';
import 'package:poster_interaktif/features/quiz_page_view.dart';
import 'package:poster_interaktif/features/quiz_result_page_view.dart';

class PosterRoutes {
  static const String home = "/home";
  static const String quiz = "$ayoMulai/quiz";
  static const String materi = "$ayoMulai/materi";
  static const String ayoMulai = "/ayoMulai";
  static const String instruction = "/instruction";
  static const String quizResult = "$ayoMulai/quizResult";
  static const String quizGuide = "$ayoMulai/quizGuide";

  static Route<dynamic> onGenerateRoute(RouteSettings settings) {
    switch (settings.name) {
      case home:
        return MaterialPageRoute(
            builder: (_) => HomePageView(), settings: settings);
      case instruction:
        InstructionType contentType = settings.arguments as InstructionType;
        return MaterialPageRoute(
            builder: (_) => InstructionPageView(contentType));
      case ayoMulai:
        return MaterialPageRoute(builder: (_) => AyoMulaiPageView());
      case materi:
        return MaterialPageRoute(builder: (_) => MateriPageView());
      case quiz:
        return MaterialPageRoute(builder: (_) => QuizPageView());
      case quizResult:
        QuizResultArgument argument = settings.arguments;
        return MaterialPageRoute(
          builder: (_) => QuizResultPageView(quizResultArgument: argument,),
        );
      case quizGuide:
        return MaterialPageRoute(
          builder: (_) => QuizGuidePageView()
        );
      default:
        return MaterialPageRoute(
          builder: (_) => Scaffold(
            body: Center(
              child: Text("Not permitted route"),
            ),
          ),
        );
    }
  }
}
